#include<bits/stdc++.h>
using namespace std;

int totalFruit(vector<int>& tree) {
        unordered_map<int,int> m;
        int size=tree.size();
        int l=0,r=0,count=0, bt=0;

        while(r<size){
            if(m[tree[r]]==0) 
                bt++;
            
            m[tree[r]]++;
            r++;
            
            while(l<r && bt>2){
                m[tree[l]]--;
                if(m[tree[l]]==0) bt--;
                l++;
            }
            
            count=max(count,r-l);
        }
        return count;
    }

int main(){
    vector<int> tree = {3,3,3,1,2,1,1,2,3,3,4};
    cout<<totalFruit(tree);
    return 0;

}