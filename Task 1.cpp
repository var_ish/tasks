#include<bits/stdc++.h>
using namespace std;

char findNewCharacter(string s, string t)
{
    // Logic - We will insert all the characters of the second string in an empty Hash Table.
    unordered_map<char, int> ump;

    // Storing the second string in map with frequency.
    for (int i = 0; i < t.length(); i++)
        ump[t[i]]++;
  
    // Removing the characters which are present in the first string from the map.
    for (int i = 0; i < s.length(); i++)
        ump[s[i]]--;
    
    // If frequency is 1, then it is the added character.
    for (auto x = ump.begin(); x != ump.end(); x++) {
        if (x->second == 1)
            return x->first;
    }
}

int main(){
    string s, t;
    cin>>s>>t;
    cout << findNewCharacter(s, t);
    return 0;
}